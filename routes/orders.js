var express = require('express');
var router = express.Router();
var Parse = require('parse').Parse;

Parse.initialize("kcAt6RkWQy2Iqzoq83iH9aEL93pFhxq7Ox9cu1PT", "ORXiYhOkkvkAjrQUneYKrxTa0zeETuRXxY9UiMQW");
/* GET users listing. */
router.get('/', function(req, res, next) {


  
	var orderJSON = {};
	var orderArray = [];

	var orderObject = Parse.Object.extend("Order");
	var query = new Parse.Query(orderObject);
	query.include("productPointer");
	query.include("boutiquePointer");
	query.include("discountPointer");
	query.descending("orderDateTime");
	query.limit(50);
	query.find({
		
		  success: function(results) {
		  	var k = 0;
		    console.log("Successfully retrieved " + results.length + " scores.");
		    // Do something with the returned Parse.Object values
		    for (var i = 0; i < results.length; i++) {
		      var object = results[i];
		      console.log(results.length);
		      orderJSON = {};
		      orderJSON.netPrice = object.get('netPrice');
		      orderJSON.totalPrice = object.get('totalPrice');
		      orderJSON.orderId = object.get('orderId');
		      orderJSON.orderDateTime = object.get('orderDateTime');
		      orderJSON.orderStatus = object.get('orderStatus');

				if (!(object.get('discountPointer') === undefined))
				{
					console.log(object.get('discountPointer').get('discountCode') );
					orderJSON.discountCode = object.get('discountPointer').get('discountCode');

				}
		      console.log(object.get('productPointer').get('productName'));
		      orderJSON.productName = object.get('productPointer').get('productName');

		      console.log(object.get('boutiquePointer').get('boutiqueName'));
		      orderJSON.boutiqueName = object.get('boutiquePointer').get('boutiqueName');
		      orderArray.push(orderJSON);
		      
		    }

		    	res.render('orders', { orderArray : orderArray , pageId : 1 });
		    	console.log(orderArray);
		  },
		  error: function(error) {
		    res.send("Error: " + error.code + " " + error.message);
		  }
	});



});


router.post('/searchItem', function(req, res, next) {

	var orderJSON = {};
	var orderArray = [];

	var inputSearch = req.body.inputSearch;

	console.log(inputSearch);

	var orderObject = Parse.Object.extend("Order");
	var query = new Parse.Query(orderObject);
	query.equalTo("orderId", inputSearch);
	query.include("productPointer");
	query.include("boutiquePointer");
	query.include("discountPointer");
	query.descending("orderDateTime");
	query.find({
		
		  success: function(results) {
		  	var k = 0;
		    console.log("Successfully retrieved " + results.length + " scores.");
		    // Do something with the returned Parse.Object values
		    for (var i = 0; i < results.length; i++) {
		      var object = results[i];

		      orderJSON = {};
		      orderJSON.netPrice = object.get('netPrice');
		      orderJSON.totalPrice = object.get('totalPrice');
		      orderJSON.orderId = object.get('orderId');
		      orderJSON.orderDateTime = object.get('orderDateTime');
		      orderJSON.orderStatus = object.get('orderStatus');

				if (!(object.get('discountPointer') === undefined))
				{
					console.log(object.get('discountPointer').get('discountCode') );
					orderJSON.discountCode = object.get('discountPointer').get('discountCode');

				}
		      console.log(object.get('productPointer').get('productName'));
		      orderJSON.productName = object.get('productPointer').get('productName');

		      console.log(object.get('boutiquePointer').get('boutiqueName'));
		      orderJSON.boutiqueName = object.get('boutiquePointer').get('boutiqueName');
		      orderArray.push(orderJSON);
		      
		    }

		    	res.render('orders', { orderArray : orderArray , pageId : 1 });
		    	console.log(orderArray);
		  },
		  error: function(error) {
		    res.send("Error: " + error.code + " " + error.message);
		  }
	});

});


router.get('/:pageId', function(req, res, next) {

  var pageIdString = req.params.pageId;
  var pageId = parseInt(pageIdString);
  var skipCounter = 50 * pageId;

  pageId = pageId + 1;

  
	var orderJSON = {};
	var orderArray = [];

	var orderObject = Parse.Object.extend("Order");
	var query = new Parse.Query(orderObject);
	query.include("productPointer");
	query.include("boutiquePointer");
	query.include("discountPointer");
	query.descending("orderDateTime");
	query.limit(50);
	query.skip(skipCounter);
	query.find({
		
		  success: function(results) {
		  	var k = 0;
		    console.log("Successfully retrieved " + results.length + " scores.");
		    // Do something with the returned Parse.Object values
		    for (var i = 0; i < results.length; i++) {
		      var object = results[i];
		      console.log(results.length);
		      orderJSON = {};
		      orderJSON.netPrice = object.get('netPrice');
		      orderJSON.totalPrice = object.get('totalPrice');
		      orderJSON.orderId = object.get('orderId');
		      orderJSON.orderDateTime = object.get('orderDateTime');
		      orderJSON.orderStatus = object.get('orderStatus');

				if (!(object.get('discountPointer') === undefined))
				{
					console.log(object.get('discountPointer').get('discountCode') );
					orderJSON.discountCode = object.get('discountPointer').get('discountCode');

				}
		      console.log(object.get('productPointer').get('productName'));
		      orderJSON.productName = object.get('productPointer').get('productName');

		      console.log(object.get('boutiquePointer').get('boutiqueName'));
		      orderJSON.boutiqueName = object.get('boutiquePointer').get('boutiqueName');
		      orderArray.push(orderJSON);
		      
		    }

		    	res.render('orders', { orderArray : orderArray , pageId : pageId });
		    	console.log(orderArray);
		  },
		  error: function(error) {
		    res.send("Error: " + error.code + " " + error.message);
		  }
	});






});

module.exports = router;
